package com.bskyb.internettv.parental_control_service

trait ParentalControlService {

//  a ordered list is used to implement the parental control levels
  lazy val parentalControlLevels = List("U", "PG", "12", "15", "18")

  def canWatchMovie (customerParentalControlLevel: String, movieId: String): Either[String, Boolean]

}
