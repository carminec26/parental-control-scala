package com.bskyb.internettv.parental_control_service

import com.bskyb.internettv.thirdparty.{MovieService, MovieServiceException}

class ParentalControlServiceImpl(movieService: MovieService) extends ParentalControlService {

  override def canWatchMovie(customerParentalControlLevel: String, movieId: String): Either[String, Boolean] = {

    val movieServiceResponse = movieService.getParentalControlLevel(movieId)

    movieServiceResponse match {
      case Right(response: String) => Right(checkParentalControl(response, customerParentalControlLevel))
      case Left(error: MovieServiceException) => Left(error.message)
    }

  }

  private def checkParentalControl(movieServiceResponse: String, customerParentalControlLevel: String): Boolean = {

//    the comparison is done using the position of customer prederence and movie level in the list of parental control levels
    val movieLevel = parentalControlLevels.indexOf(movieServiceResponse)
    val customerLevel = parentalControlLevels.indexOf(customerParentalControlLevel)

    if(movieLevel <= customerLevel)
      true
    else
      false
  }

}
