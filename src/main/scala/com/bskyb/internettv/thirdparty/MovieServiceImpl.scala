package com.bskyb.internettv.thirdparty

class MovieServiceImpl extends MovieService {

  override def getParentalControlLevel(titleId: String): Either[MovieServiceException, String] = {

    titleId match {
      case "1000" => Right("15")
      case "1001" => Right("12")
      case "2000" => Left(new TechnicalFailureException)
      case _ => Left(new TitleNotFoundException)
    }

  }

}
