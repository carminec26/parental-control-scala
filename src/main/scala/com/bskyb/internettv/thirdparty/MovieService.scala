package com.bskyb.internettv.thirdparty

trait MovieService {

  def getParentalControlLevel (titleId: String): Either[MovieServiceException, String]

}

trait MovieServiceException {
  val message: String
}

case class TitleNotFoundException(message: String = "We could not find the movie you requested") extends MovieServiceException

case class TechnicalFailureException(message: String = "We are experiencing technical issues") extends MovieServiceException