package com.bskyb.internettv.parental_control_service

import com.bskyb.internettv.thirdparty.{MovieService, MovieServiceImpl}
import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class ParentalControlServiceImplTest extends FeatureSpec with GivenWhenThen with Matchers with BeforeAndAfterEach {

  val movieService: MovieService = new MovieServiceImpl()
  val parentalControlService: ParentalControlService = new ParentalControlServiceImpl(movieService)

  feature("Customers can access the movies based on their parental control preferences") {

    scenario("A customer wants to watch a movie with a level equals to the parental control preference") {

      Given("the customer parental control preference and a movie title")
      val preference = "15"
      val movieId = "1000"

      When("the customer requests to watch a movie with level equals to his preference")
      val response = parentalControlService.canWatchMovie(preference, movieId)

      Then("the customer is allowed to watch the movie")
      response.isRight should be (true)
      response.right.get should be (true)

    }

    scenario("A customer wants to watch a movie with a level lower than parental control preference") {

      Given("the customer parental control preference and a movie title")
      val preference = "15"
      val movieId = "1001"

      When("the customer requests to watch a movie with level lower than preference")
      val response = parentalControlService.canWatchMovie(preference, movieId)

      Then("the customer is allowed to watch the movie")
      response.isRight should be (true)
      response.right.get should be (true)

    }

    scenario("A customer wants to watch a movie with a level higher than parental control preference") {

      Given("the customer parental control preference and a movie title")
      val preference = "PG"
      val movieId = "1000"

      When("the customer requests to watch a movie with level higher than preference")
      val response = parentalControlService.canWatchMovie(preference, movieId)

      Then("the customer is not allowed to watch the movie")
      response.isRight should be (true)
      response.right.get should be (false)

    }

    scenario("A customer wants to watch a movie that is not present") {

      Given("the customer parental control preference and a movie title")
      val preference = "PG"
      val movieId = "1002"

      When("the customer requests to watch a movie that is not present")
      val response = parentalControlService.canWatchMovie(preference, movieId)

      Then("the customer receive a message title not found")
      response.isLeft should be (true)
      response.left.get should be ("We could not find the movie you requested")

    }

    scenario("A customer wants to watch a movie but a TechnicalFailureException is thrown") {

      Given("the customer parental control preference and a movie title")
      val preference = "PG"
      val movieId = "2000"

      When("the customer requests to watch a movie")
      val response = parentalControlService.canWatchMovie(preference, movieId)

      Then("the customer receive a message technical issues")
      response.isLeft should be (true)
      response.left.get should be ("We are experiencing technical issues")

    }

  }

}
